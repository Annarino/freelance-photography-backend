const multer = require('multer');
const path = require('path')

/**_____________________ Proposals _____________________**/
const proposalStorage = multer.diskStorage({
    
    destination: (req, file, cb) => {
        cb(null, `proposals/${req.userId.toString()}`)
    },
    filename: (req, file, cb) => {
        filename=req.body._id
        cb(null, filename+'_'+file.originalname)
    }
});

const pdfFilter = (req, file, cb) => {
    file.mimetype === 'application/pdf' ? cb(null, true) : cb(null, false);
}

exports.proposals = multer({
        storage: proposalStorage,
        fileFilter: pdfFilter
    }).single('proposal');


/**_____________________ Images _____________________**/
const imageStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'tmp/images')
    },
    filename: (req, file, cb) => {
        cb(null, req.userId+"_"+file.originalname)
    }
});
const imageFilter = (req, file, cb) => {
    if(
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ){
        cb(null, true);
    } else {
        cb(null, false);
    }
}
exports.images = multer({
    storage: imageStorage,
    fileFilter: imageFilter
}).array('image', 10)