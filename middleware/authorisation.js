const jwt = require('jsonwebtoken');

const globals = require('../globals');

exports.authorize = (req, res, next) => {
    let decodedToken;
    if(!req.get('Authorization')){
        return res.status(401).json({message: 'Not authorized.'});
    }
    try{
        const token = req.get('Authorization').split(' ')[1];
        decodedToken = jwt.verify(token, 'DEVELOPMENT SECRET KEY TO CHANGE')
    } catch(err){
        err.statusCode = 500;
        res.status(500).json(err)
        throw err;
    }
    if(!decodedToken){
        const err = new Error({
            statusCode: 401
        });
        res.status(401).json(err);
        throw err;
    }
    req.userId = decodedToken.userId;
    req.userType = decodedToken.userType;
    req.status = decodedToken.status;
    return next();
}

exports.isPendingSupplier = (req, res, next) => {
    if(req.userType === globals.supplier){
        return next();
    } else {
        return res.status(401).json({statusCode: 401, message: "Not authorised."});
    }
}

exports.isSupplier = (req, res, next) => {
    if(req.userType === globals.supplier && req.status === globals.accepted){
        return next();
    } else {
        return res.status(401).json({statusCode: 401, message: "Not authorised."});
    }
}

exports.isClient = (req, res, next) => {
    if(req.userType === globals.client){
        return next()
    } else {
        return res.status(401).json({statusCode: 401, message: "Not authorised."});
    }
}

exports.isAdmin = (req, res, next) => {
    if(req.userType === globals.admin){
        return next();
    }
    return res.status(401).json({statusCode: 401, message: "Not authorised."});
}

exports.isAdminOrClient = (req, res, next) => {
    if(req.userType === globals.admin || req.userType === globals.client){
        return next();
    }
    return res.status(401).json({statusCode: 401, message: "Not authorised."});
}

exports.isAdminOrSupplier = (req, res, next) => {
    if(req.userType === globals.admin){
       return next();
    } 
    if (req.userType === globals.supplier && req.status === globals.accepted){
        return next();
    }
    return res.status(401).json({statusCode: 401, message: "Not authorised."});
}

exports.isSupplierOrClient = (req, res, next) => {
    if(req.userType === globals.client){
        return next();
    }
    if (req.userType === globals.supplier && req.status === globals.accepted){
        return next();
    }
    return res.status(401).json({statusCode: 401, message: "Not authorised."});
}