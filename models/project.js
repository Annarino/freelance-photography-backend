const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const projectSchema = new Schema({
    
    clientId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        req: true
    },
    supplierBids: [
        {
            supplierId: {
                type: Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            status:{
                type: String,
                default: 'pending'
            }
        }   
    ],
    status: {
        type: String,
        required: true
    },
    dateCreated: {
        type: Date,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate:{
        type: Date,
        required: true
    },
    location: {
        type: String,
        required: false
    },
    projectType: {
        type: String,
        required: false
    },
    budgetMin: Number,
    budgetMax: Number,
    description: String,
    equipment: [
        {
            equipmentId: {
                type: Schema.Types.ObjectId,
                ref: 'Equipment',
                required: false
            }
        }
    ],
    expenses: [
        {
            expenseId: {
                type: Schema.Types.ObjectId,
                ref: 'Expense',
                required: false
            }
        }
    ],
    notes: String
});

module.exports = mongoose.model('Project', projectSchema);