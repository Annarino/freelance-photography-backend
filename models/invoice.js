const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const invoiceSchema = new Schema({
    projectId:{
        type: Schema.Types.ObjectId,
        ref: 'Project',
        required: false
    },
    clientId: Schema.Types.ObjectId,
    supplierId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    invoiceNumber: String,
    issueDate:{
        type: Date,
        required: true
    },
    supplyDate:{
        type: Date,
        required: true
    },
    paymentWindow:{
        type: Number,
        required: true
    },
    paymentDate: Date,
    VATNum:{
        type: Number,
        required: false
    },
    clientInformation: {
        opened:{
            type: Boolean,
            default: false
        },
        firstName: {
            type: String,
            required: true
        },
        surname: {
            type: String,
            required: true
        },
        email: String,
        companyName: String,
        logoURL: String,
        address: {
            streetNumName:{
                type: String,
                required: true
            },
            houseNumName:{
                type: String,
                required: true
            },
            city:{
                type: String,
                required: true
            },
            province:{
                type: String,
                required: false
            },
            country:{
                type: String,
                required: true
            }
        }
    },
    supplierInformation: {
        firstName: {
            type: String,
            required: true
        },
        surname: {
            type: String,
            required: true
        },
        companyName: {
            type: String,
            required: false
        },
        phoneNumber: {
            type: Number,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        logoURL:{
            type: String,
            required: false
        }
    },
    item:[
        {
            description: {
                type: String,
                required: true
            },
            VATRate: {
                type: Number,
                default: 0
            },
            amount: {
                type: Number,
                default: 0
            }
        }
    ],
    subtotal:{
        type: Number,
        required: true
    },
    VATTotal:{
        type: Number,
        required: true
    },
    total:{
        type: Number,
        required: true
    },
    status: {
        type: String,
        default: 'unpaid'
    },
    sentToClient: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Invoice', invoiceSchema);