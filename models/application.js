const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const applicationSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    userType: {
        type: String,
        required: true  
    },
    firstName: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    address: {
        streetNumName:{
            type: String,
            required: true
        },
        houseNumName:{
            type: String,
            required: true
        },
        city:{
            type: String,
            required: true
        },
        province: String,
        country:{
            type: String,
            required: true
        },
        postcode:{
            type: String,
            required: true
        }
    },
    references: [
        {
            name: {
                type: String,
                required: true
            },
            email: {
                type: String,
                required: true
            },
            phoneNumber: Number,
            notes: String
        }
    ],
    yearsExperience: {
        type: Number,
        required: true
    },
    portfolioLinks: [
        {
            link: String
        }
    ],
    notes: String,
    status: String
});

module.exports = mongoose.model('Application', applicationSchema);