const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    byUserId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    forUserId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    rating: [
        {
            ratingParam: {
                type: String,
                required: true
            },
            score: {
                type: mongoose.Types.Decimal128,
                min:0.00,
                max:10.00,
                required: true
            }
        }
    ],
    originalReview: String,
    updatedReview: String,
    reviewDate: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Review', reviewSchema);