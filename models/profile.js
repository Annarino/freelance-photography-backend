const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const profileSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    userType:{
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    surname:{
        type: String,
        required: true
    },
    companyName:{
        type: String,
        required: false
    },
    VATNum: String,
    logoURL:{
        type: String,
        required: false
    },
    profilePhotoURL:{
        type: String,
        default: "https://semantic-ui.com//images/avatar2/large/elyse.png"
    },
    email:{
        type: String,
        required: true
    },
    phoneNumber:{
        type: Number,
        required: true
    },
    biography: String,
    address: {
        streetNumName:{
            type: String,
            required: true
        },
        houseNumName:{
            type: String,
            required: true
        },
        city:{
            type: String,
            required: true
        },
        province:{
            type: String,
            required: false
        },
        country:{
            type: String,
            required: true
        },
        postcode:{
            type: String,
            required: true
        }
    },
    albums:[
        {
            albumId:{
                type: Schema.Types.ObjectId,
                ref: 'Album',
                required: true
            }
        }
    ],
    images:[
        {
            imageId: {
                type: Schema.Types.ObjectId,
                ref: 'Image',
                required: true
            },
            imageUrl: {
                type: String,
                required: true
            }
        }
    ]
    
});

module.exports = mongoose.model('Profile', profileSchema);