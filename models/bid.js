const mongoose = require('mongoose');
const globals = require('../globals');

const Schema = mongoose.Schema;

const bidSchema = new Schema({
    supplierId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    supplierProfileId:{
        type: Schema.Types.ObjectId,
        ref:'Profile',
        required: true
    },
    projectId:{
        type: Schema.Types.ObjectId,
        ref: 'Project',
        required: true
    },
    status:{
        type: String,
        default: globals.pending
    },
    price: Number,
    submissionDate: Date,
    proposalURL: {
        type: String,
        required: true
    },
    message: String
});

module.exports = mongoose.model('Bid', bidSchema);