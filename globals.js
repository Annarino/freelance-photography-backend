//User Types
exports.supplier = 'supplier';
exports.client = 'client';
exports.administrator = 'administrator';

//Statuses (for applications, projects, etc)
exports.pending = 'pending';
exports.accepted = 'accepted';
exports.denied = 'denied';
exports.open = 'open';
exports.closed = 'closed';
exports.complete = 'complete';
exports.paid = 'paid';
exports.unpaid = 'unpaid';
exports.pastDue = 'past due'
