const express = require('express');

const review = require('../controllers/reviews');

const router = express.Router();



router.get('/:reviewId', review.getReview);
router.get('/for-user/:userId', review.getReviewsForUser);
router.get('/by-user/:userId', review.getReviewsByUser);

router.post('/', review.addReview);

router.put('/:reviewId', review.editReview);

router.delete('/:reviewId', review.deleteReview);

module.exports = router;

