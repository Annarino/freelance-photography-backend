const express = require('express');
const auth = require('../middleware/authorisation');

const profile = require('../controllers/profiles');

const router = express.Router();

router.get('/type/:type',auth.authorize, profile.getProfilesByType)
router.get('/:profileId',auth.authorize, profile.getProfile)
router.get('/', auth.authorize, profile.getUserProfile)

router.post('/', profile.addProfile);

router.put('/:profileId',auth.authorize, auth.isAdmin, profile.editProfile);
router.put('/', auth.authorize, auth.isSupplierOrClient, profile.editUserProfile);

router.delete('/:profileId', profile.deleteProfile);

module.exports = router;