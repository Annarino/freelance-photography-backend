const path =  require('path');

const express = require('express');
const auth = require('../middleware/authorisation');
const globals = require('../globals');

const invoice = require('../controllers/invoices');

const router = express.Router();

router.get('/:invoiceId', auth.authorize, auth.isSupplierOrClient, invoice.getInvoice);
router.get('/', auth.authorize, auth.isSupplierOrClient, invoice.getInvoices);

router.post('/', auth.authorize, auth.isSupplier, invoice.addInvoice);

router.put('/:invoiceId',auth.authorize, invoice.editInvoice);

router.delete('/:invoiceId', auth.authorize, auth.isAdminOrSupplier, invoice.deleteInvoice);

module.exports = router;