const express = require('express');

const auth = require('../middleware/authorisation');

const bid = require('../controllers/bids');

const router = express.Router();



router.get('/project/:projectId', auth.authorize, auth.isAdminOrClient, bid.getProjectBids);
router.get('/supplier', auth.authorize, auth.isAdminOrSupplier, bid.getSupplierBids);
router.get('/:bidId', auth.authorize, bid.getBid);

router.post('/', auth.authorize, auth.isSupplier, bid.addBid);

router.put('/:bidId', auth.authorize, bid.editBid);

router.delete('/:bidId', auth.authorize, auth.isAdminOrSupplier, bid.deleteBid);

module.exports = router;