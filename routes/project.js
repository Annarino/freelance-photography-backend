const path = require('path');

const express = require('express');

const project = require('../controllers/projects');
const auth = require('../middleware/authorisation');

const router = express.Router();

router.get('/users-projects', auth.authorize, project.getUsersProjects);

router.get('/bookings/:bidId', auth.authorize, auth.isAdminOrSupplier, project.getBooking);
router.get('/bookings', auth.authorize, auth.isAdminOrSupplier, project.getBookings);
router.get('/project/:projectId', auth.authorize, project.getProject);
router.get('/', project.getProjects);

router.post('/', auth.authorize, project.addProject);

router.put('/:projectId', auth.authorize, auth.isAdminOrClient, project.editProject);

router.delete('/:projectId', auth.authorize, auth.isAdminOrClient, project.deleteProject);

module.exports = router;