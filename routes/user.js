const path =  require('path');

const express = require('express');
const auth = require('../middleware/authorisation');

const user = require('../controllers/users');

const router = express.Router();


router.get('/user', auth.authorize, user.getUserSelf);
router.post('/client', auth.authorize, auth.isAdminOrSupplier, user.findClientByEmail)
router.get('/:userId', auth.authorize, auth.isAdmin, user.getUser);
router.get('/user-type/:userType', auth.authorize, auth.isAdmin, user.getUsersOfType);

router.post('/signup', user.addUser);
router.post('/login', user.loginUser);
router.post('/administrator', auth.authorize, auth.isAdmin, user.addAsAdministrator);

router.delete('/:userId', auth.authorize, auth.isAdmin, user.deleteUser);


module.exports = router;