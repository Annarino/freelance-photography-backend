const express = require('express');
const auth = require('../middleware/authorisation');

const application = require('../controllers/applications');

const router = express.Router();

router.get('/application-type/:applicantType',auth.authorize, auth.isAdmin, application.getApplicationsOfType);
router.get('/supplier-application', auth.authorize, auth.isPendingSupplier, application.getSupplierApplication);
router.get('/:applicationId',auth.authorize, auth.isAdmin, application.getApplication);
router.get('/', auth.authorize, auth.isAdmin, application.getApplications);

router.post('/',auth.authorize, auth.isPendingSupplier, application.addApplication);

router.put('/:applicationId', auth.authorize, auth.isSupplier, application.editApplication);

router.delete('/:applicationId', auth.authorize, auth.isAdmin, application.deleteApplication);

module.exports = router;