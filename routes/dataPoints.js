const express = require('express');
const router = express.Router();

const dataPoints = require('../controllers/dataPoints')

router.get('/revenue/:vatIncludedExcluded', dataPoints.getRevenue)

module.exports = router;