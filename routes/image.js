const express = require('express');
const path = require('path');
const auth = require('../middleware/authorisation');
const fileHandlers = require('../middleware/fileHandlers');

const image = require('../controllers/images');

const router = express.Router();

router.get('/supplier/:supplierId', image.getUserImages)
router.get('/image-library',auth.authorize, image.getImageLibrary)

//Change when authorisation is in place!
router.post('/',auth.authorize, fileHandlers.images, image.addImage);


module.exports = router;