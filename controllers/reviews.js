const mongoose = require('mongoose');

const errors = require('./error');

const Review =  require('../models/review');

exports.addReview = (req, res, next) => {
    const review = new Review(req.body);
    Review
        .findOne({reviewerId: review.reviewerId, revieweeId: review.revieweeId})
        .then(reviewExists => {
            if(reviewExists){
                const error = new Error();
                error.statusCode = 409;
                error.message = "A review with the current combination of reviewer and reviewee already exists."
                throw error;
            }
            return review;
        })
        .then(newReview =>{
            return newReview.save()
        })
        .then(review => res.status(201).send(review))
        .catch(err => errors.responseError(err, res));
}

exports.getReview = (req, res, next) => {
    const reviewId = mongoose.Types.ObjectId(req.params.reviewId);
    Review  
        .findById(reviewId)
        .then(review => res.status(200).send(review))
        .catch(err => errors.responseError(err, res));
}

//Returns the reviews for a user.
exports.getReviewsForUser = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.params.userId);
    Review
        .find({forUserId: userId})
        .then(reviews => res.status(200).send(reviews))
        .catch(err => errors.responseError(err, res));
}

//Returns the reviews made by a user.
exports.getReviewsByUser = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.params.userId);
    Review
        .find({byUserId: userId})
        .then(reviews => res.status(200).send(reviews))
        .catch(err =>  errors.responseError(err, res));
}

exports.editReview = (req, res, next) => {
    const reviewId = mongoose.Types.ObjectId(req.params.reviewId);
    Review
        .findByIdAndUpdate(reviewId)
        .then(review => { return Review.findById(reviewId)})
        .then(updatedReview => res.status(200).send(updatedReview))
        .catch(err => errors.responseError(err, res));
}

exports.deleteReview = (req, res, next) => {
    const reviewId = mongoose.Types.ObjectId(req.params.reviewId);
    Review
        .findByIdAndDelete(reviewId)
        .then(deltedReview => {
            if(!deletedReview){
                const error = new Error();
                error.statusCode = 404;
                error.message = "Could not find the review to delete.";
                throw error;
            }
            res.status(200).send(deletedReview);
        })
        .catch(err => errors.responseError(err, res));
}