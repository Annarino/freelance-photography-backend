const mongoose = require('mongoose');
const fs = require('fs')

const errors = require('./error');

const Profile = require('../models/profile');
const User = require('../models/user')

const globals = require('../globals')

exports.addProfile = (req, res, next) => {
    const ProfileId = req.body.ProfileId;
    const profile =  req.body;
    if(req.userType === globals.supplier){
        fs.mkdirSync(`/proposals/${req.userId.toString()}`)
        fs.mkdirSync(`/images/${req.userId.toString()}`)
    }
    Profile
        .findOne({ProfileId})
        .then(profile => {
            if(profile){
                const error = new Error();
                error.statusCode = 409;
                error.message = "A profile for this Profile already exists!";
            }
            return profile.save();
        })
        .then(addedProfile => {
            let updateUser = new User;
            updateUser.profile = addedProfile._id
        })
        .then(user => res.status(201).send(addedProfile))
        .catch(err => {
            errors.responseError(err, res);
        });
}

exports.editProfile = (req, res, next) => {
    const profile = req.body;
    const profileId = req.params.profileId;

    Profile
        .findByIdAndUpdate(profileId)
        .then(profile => {
            if(!profile){
                const error = new Error();
                error.statusCode = 404;
                error.message = "Profile was not updated because the Profile could not be found."
                throw error;
            }
            return Profile.findById(profileId);
        })
        .then(updatedProfile => res.status(200).send(updatedProfile))
        .catch(err => {
            errors.responseError(err, res);
        });
}

exports.editUserProfile = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.userId);
    const editedProfile = new Profile(req.body)
    editedProfile.userType = req.userType;
    editedProfile.userId = req.userId;
    Profile
        .findOne({userId})
        .then(profile => {
            if(!profile){
                return editedProfile.save();
            }
            return Profile.findOneAndUpdate({userId}, editedProfile, {new:true, useFindAndModify: false})
        })
        .then(updatedProfile => {
            return res.status(200).send(updatedProfile)
        })
        .catch(err => errors.responseError(err, res))
}

exports.getProfile = (req, res, next) => {
    console.log('params: ',req.params)
    const profileId = mongoose.Types.ObjectId(req.params.profileId);
    Profile
        .findById(profileId)
        .then(profile => {
            if(!profile){
                const err = new Error();
                error.statusCode = 404;
                error.message = "Could not find this Profile."
                throw error;
            }
            res.status(200).send(profile);
        })
        .catch(err => {
            console.log('User profile: ', err)
            errors.responseError(err, res)
        });
}

exports.getUserProfile = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.userId);
    
    Profile
        .findOne({userId})
        .then(profile => {
            if(!profile){
                const err = new Error();
                    err.statusCode = 404;
                    err.message = 'Could not find a profile for this user.'
                throw err;
            }
            return res.status(200).send(profile);
        })
        .catch(err => {
            console.log('user own profile err: ',err)
            errors.responseError(err, res)});
}

exports.getProfilesByType = (req, res, next) => {
    //need to add pagination to limit amount returned at once
    Profile
        .find({userType: req.params.type})
        .then(profiles => {
            if(!profiles){
                const error = new Error();
                error.statusCode = 404;
                error.message = "Could not find any profiles of this type."
            }
            res.status(200).send(profiles)
        })
        .catch(err => errors.responseError(err, res));
}

exports.deleteProfile = (req, res, next) => {
    ProfileId = mongoose.Types.ObjectId(req.params.ProfileId);
    Profile
        .findByIdAndDelete(ProfileId)
        .then(deletedProfile =>{
            if(!deletedProfile){
                const error = new Error();
                error.statusCode = 404;
                error.message = "Could not find the Profile to delete."
                throw error;
            }
            res.status(200).send(deletedProfile);
        })
        .catch(err => errors.responseError(err, res))
}