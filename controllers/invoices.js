const mongoose = require('mongoose');
const error = require('./error');
const calcs = require('./calculations');
const globals = require('../globals');

const Invoice = require('../models/invoice');
const Project = require('../models/project');
const User = require('../models/user');

exports.getInvoice = (req, res, next) => {
    const authUserId = mongoose.Types.ObjectId(req.userId);
    const authUserType = req.userType;
    const invoiceId = mongoose.Types.ObjectId(req.params.invoiceId);
    Invoice
        .findById(invoiceId)
        .then( invoice => {
            if(!invoice){
                const err = new Error({
                    statusCode: 404,
                    message: 'Could not find the requested invoice.'
                });
                throw err;
            }
            if(authUserId !== invoice.clientId && authUserId !== invoice.supplierId){
                const err = new Error({
                    statusCode: 401,
                    message: 'You are not authorised to view this invoice.'
                });
                throw err;
            }
            res.status(200).send(invoice);
        })
        .catch(err => error.responseError(err, res));
}

exports.getInvoices =  (req, res, next) => {
    const authUserType = req.userType;
    const authUserId = req.userId;
    if(authUserType  === globals.supplier){
        Invoice
            .find({supplierId: authUserId})
            .then(invoices => res.status(200).send(invoices))
            .catch(err => error.responseError(err, res));
    } else if (authUserType === globals.client){
        Invoice
            .find({clientId: authUserId})
            .then(invoices => res.status(200).send(invoices))
            .catch(err => error.responseError(err, res));
    } else{
        const err = {
            statusCode: 401,
            message: 'Not authorised.'
        }
        return res.status(401).json(err);
    }
    
};

exports.addInvoice = (req, res, next) => {
    const invoice = new Invoice(req.body);
    invoiceNumber = invoice._id 
    invoice.supplierId = req.userId;
    invoice.invoiceNumber = invoiceNumber;
    invoice.issueDate = new Date().toISOString();
    invoice.status = globals.unpaid;
    User
        .findOne({userType: globals.client, email: invoice.clientInformation.email})
        .then(client => {
            if(client){
                invoice.clientId = client._id
            }
            invoice.subtotal = calcs.subtotal(invoice.item);
            invoice.VATTotal = calcs.vatTotal(invoice.item);
            invoice.total = calcs.total(invoice.item);
            return invoice.save()
        })
        .then(result => res.status(200).send(result)) 
        .catch(err => {
            console.log('addINvoice: ',err)
            res.status(500).send(err)}); 
};

exports.editInvoice = (req, res, next) => {
    const invoiceId = mongoose.Types.ObjectId(req.params.invoiceId);
    if(req.userType === globals.client){
        Invoice
            .findById({_id: invoiceId})
            .then(invoice => {
                if(!invoice){
                    const err = new Error();
                    err.statusCode = 404
                    throw err;
                }
                if(invoice.status !== 'paid' && req.body.status === 'paid'){
                    invoice.paymentDate = new Date();
                }
                invoice.status = req.body.status;
                return Invoice.findByIdAndUpdate(invoiceId, invoice, {new: true, useFindAndModify: false})
            })
            .then(updatedInvoice => {
                if(!updatedInvoice){
                    const err = new Error({
                        statusCode: 404,
                        message: 'Cannot find the updated invoice.'
                    })
                    throw err;
                }
                return res.status(200).send(updatedInvoice);
            })
            .catch(err => error.responseError(err, res));
            return;
    }
    Invoice
        .findById(invoiceId)
        .then(invoice => {
            console.log(invoice.status)
            console.log(req.body.status)
            if(invoice.status !== 'paid' && req.body.status === 'paid'){
                req.body.paymentDate = new Date();
            }
            return Invoice.findByIdAndUpdate(invoiceId, req.body, {new: true, useFindAndModify: false})
        })
        .then(updatedInvoice => {
            if(!updatedInvoice){
                const err = new Error({
                    statusCode: 404,
                    message: 'Cannot find the updated invoice.'
                })
                throw err;
            }
            res.status(200).send(updatedInvoice);
        })
        .catch(err => error.responseError(err, res));
}

exports.deleteInvoice = (req, res, next) => {
    const invoiceId = mongoose.Types.ObjectId(req.params.invoiceId);
    const userId = mongoose.Types.ObjectId(req.userId);
    Invoice
        .findById(invoiceId)
        .then(invoice => {
            if(invoice.supplierId.toString() !== req.userId){
                const err = new Error({
                    statusCode: 401,
                    message: 'You do not have permission to delete this invoice'
                });
                throw err;
            }
            return Invoice.findByIdAndDelete(invoiceId);
        })
        .then(deletedInvoice => res.status(200).send(deletedInvoice))
        .catch(err =>{ 
            console.log(err)
            error.responseError(err, res)})
}