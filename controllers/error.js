function responseError(err, res){
    console.log(JSON.stringify(err))
    if(!err.statusCode){
        err.statusCode = 500;
    }
    if(!err.message){
        err.message = "There was an error processing your request."
    }
    res.status(err.statusCode).send({message: err.message});
}



exports.responseError = responseError;