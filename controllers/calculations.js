exports.vatTotal = items =>{
    let vatTotal = 0;
    items.forEach(item => {
        vatTotal += item.amount*(item.VATRate/100)
    })
    return vatTotal;
}

exports.subtotal = items =>{
    let subtotal = 0;
    items.forEach(item => {
        subtotal += item.amount;
    })
    return subtotal;
}

exports.total = items =>{
    return (this.subtotal(items) + this.vatTotal(items))
    
}

