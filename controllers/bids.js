const mongoose = require('mongoose');
const path = require('path')

const error = require('./error');
const globals = require('../globals');

const Bid = require('../models/bid');
const Project = require('../models/project');
const User = require('../models/user');

exports.getProjectBids = (req, res, next) => {
    const authUserId= mongoose.Types.ObjectId(req.userId);
    const authUserType = req.userType;
    const projectId = mongoose.Types.ObjectId(req.params.projectId);

    Project
        .findById(projectId)
        .then(project => {
            // if(project.clientId !== authUserId ){
            //     const err = new Error();
            //     err.statusCode = 401;
            //     throw err;
            // }
            return Bid.find({projectId}).populate('supplierProfileId')
        })
        .then(projectBids =>{ 
            console.log(projectBids)
            res.status(200).send(projectBids)})
        .catch(err => error.responseError(err, res));
}

exports.getSupplierBids = (req, res, next) => {
    const authUserId = req.userId;
    const authUserType = req.userType;
    Bid
        .find(({supplierId: authUserId})).populate('projectId')
        .then(supplierBids => {
            res.status(200).send(supplierBids)})
        .catch(err => error.responseError(err, res));
    
}

exports.getBid = (req, res, status) => {
    const bidId = req.params.bidId;
    const authUserType = req.userType;
    const authUserId = req.userId;
    let bid;
    Bid 
        .findById(bidId)
        .then(bid => {
            if(authUserType === globals.administrator){
                return res.status(200).send(bid);
            } else if(authUserType === globals.supplier){
                if(bid.supplierId !== authUserId){
                    const err = new Error({
                        statusCode: 401,
                        message: 'You cannot view other\' bids'
                    });
                    throw err;
                }
                return res.status(200).send(bid)
            } else if(authUserType === globals.client){
                Project
                    .findById(bid.projectId)
                    .then(project => {
                        if(project.clientId !== authUserId){
                            const err = new Error({
                                statusCode: 401,
                                message: 'You cannot view bids for this project because you are not the owner of the project.'
                            });
                            throw err;
                        }
                        return res.status(200).send(bid);
                    })
                    .catch(err => error.responseError(err, res));
            }
            else{
                const err = new Error({
                    statusCode: 401,
                    message: 'You are not authorised to view project bids.'
                });
                throw err;
            }
        })
        .catch(err => error.responseError(err, res));
}

exports.addBid = (req, res, next) => {
    const authUserId = req.userId;
    let bid;
    User
        .findById(authUserId)
        .then(user => {
            console.log('user: ',req.userId)
            console.log('filename: ', req.file.filename)
            bid = new Bid({
                supplierId: user._id,
                supplierProfileId: user.profile,
                projectId: req.body._id,
                proposalURL: `/proposals/${req.userId}/${req.file.filename}`,
                submissionDate: new Date(),
                message: req.body.notes
            });
            console.log('bid',bid)
            return Bid.findOne({supplierId: authUserId, projectId: req.body._id})
        })
        .then(project => {
            if(project){
                const err = new Error();
                err.statusCode = 409
                throw err;
            }
            return Project.findByIdAndUpdate(bid.projectId, {$push: {supplierBids: {supplierId: authUserId}}}, {useFindAndModify: false})
        })
        .then(() => {
            return bid.save()
        })
        .then(result => res.status(201).send(result))
        .catch(err => {
            console.log(err)
            error.responseError(err, res)});
}

exports.editBid = (req, res, next) => {
    const authUserType = req.userType;
    const authUserId = req.userId;
    const bidId = mongoose.Types.ObjectId(req.params.bidId);
    let bid;
    switch(authUserType){
        case globals.administrator:
            bid = req.body;
            break;
        case globals.supplier:
            bid = {
                supplierId: authUserId,
                projectId: req.body.projectId,
                proposalUrl: req.body.proposalUrl,
                message: req.body.notes
            };
            break;
        case globals.client:
            bid = {
                status: req.body.status
            }
    }
    Bid
        .findByIdAndUpdate(bidId, bid, {new: true, useFindAndModify: false})
        .then(updatedBid => {
            return res.status(200).send(updatedBid);
        })
        .catch(err =>{ 
            error.responseError(err, res)});
}

exports.deleteBid = (req, res, next) => {
    const authUserId = req.userId;
    const authUserType = req.userType;
    const bidId = mongoose.Types.ObjectId(req.params.bidId);
    Bid
        .findById(bidId)
        .then(bid => {
            if(authUserType !== globals.administrator && authUserId.toString() !== bid.supplierId.toString()){
                const err = new Error()
                err.statusCode = 401;
                throw err
            }
            return Project.findByIdAndUpdate(bid.projectId, {$pull: {supplierBids: {supplierId:authUserId}}}, {useFindAndModify: false, new:true});
        })
        .then(()=> {
            Bid.findByIdAndDelete(bidId, (err, deletedBid) =>{
                if(err){
                    const err = new Error();
                    err.statusCode = 500
                    throw err;
                }
                return deletedBid;
            })
        })
        .then(deletedBid=> res.status(200).send(deletedBid))
        .catch(err => {
            error.responseError(err, res)
        });
}