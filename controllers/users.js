const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const globals = require('../globals');

const errors = require('./error');

const User = require('../models/user');
const Profile = require('../models/profile');

exports.getUser = (req, res, next) => {
    const userId = req.params.userId;
    User
        .findByid(userId)
        .then(user => {
            if(!user){
                const err = new Error({
                    statusCode: 404,
                    message: 'Could not find user.'
                });
                throw err;
            }
            res.status(200).send(user);
        })
        .catch(err => errors.responseError(err, res));
}

exports.getUserSelf = (req, res, next) => {
    const authUserId = req.userId;
    User
        .findById(authUserId)
        .then(user => {
            if(!user){
                const err = new Error({
                    statusCode: 404,
                    message: 'Could not find user.'
                });
                throw err;
            }
            res.status(200).send(user);
        })
        .catch(err => errors.responseError(err, res));
}

exports.getUsersOfType = (req, res, next) => {
    const userType = req.params.userType;
    User
        .find({userType})
        .then(users => res.status(200).send(users))
        .catch(err => errors.responseError(err, res));
}

exports.addUser = (req, res, next) => {
    const email = req.body.email;
    const user = new User(req.body);
    if(user.userType !== globals.supplier && user.userType !== globals.client){
        const err = {
            statusCode: 401,
            message: 'A user with this type is not allowed to be created.'
        }
        return res.status(401).json(err);
    }
    User
        .findOne({email})
        .then(user => {
            if(user){
                const error = new Error();
                error.statusCode = 409;
                error.message = "A user with this email already exists!";
                throw error;
            }
            return bcrypt.hash(req.body.password, 12)
        })
        .then(pw => {
            user.password = pw;
            return user.save();
        })
        .then(user => {
            res.status(201).send(user);
        })
        .catch(err => {
            errors.responseError(err, res);
        })
}

exports.addAsAdministrator = (req, res, next) => {
    const email = req.body.email;
    const user = new user(req.body);

    User
        .findOne({email})
        .then(user => {
            if(user){
                const error = new Error();
                error.statusCode = 409;
                error.message = "A user with this email already exists!";
                throw error;
            }
            return bcrypt.hash(req.body.password, 12)
        })
        .then(pw => {
            user.password = pw;
            return user.save();
        })
        .then(user => {
            res.status(201).send(user);
        })
        .catch(err => {
            errors.responseError(err, res);
        })
}


exports.editUser = (req, res, next) => {
    const updatedUser = req.body;
    const userId = mongoose.Types.ObjectId(req.params.userId);

    User
        .findByid(userId)
        .then(user => {
            if(!user){
                const error = new Error();
                error.statusCode = 404;
                error.message = "User was not updated because the user could not be found.";
                throw error;
            }
            if(user.email !== updatedUser.email){
                return User.findOne({email: updatedUser.email});
            }
            return false;
        })
        .then(userExists => {
            if(userExists){
                const error = new Error();
                error.statusCode = 409;
                error.message = "A user with this email already exists.";
                throw error;
            }
            User.findByIdAndUpdate(userId)
        })
        .then(user => {
            if(!user){
                const error = new Error();
                error.statusCode = 404;
                error.message = "User was not updated because the user could not be found."
            }
            return user.findById(userId);
        })
        .then(updatedUser => res.status(200).send(updatedUser))
        .catch(err => {
            errors.responseError(err, res);
        });
}

exports.editUserSelf = (req, res, next) => {
    const authUserId = req.userId;
    const authUserType = req.userType;
    const user = {
        email: req.body.email,
        password: req.body.password
    }
    User
        .findById(authUserType)
        .then(originalUser => {
            if(originalUser.email !== user.email){
                return User.findOne({email: user.email})
            }
            return false;
        })
        .then(userExists => {
            if(userExists){
                const err = new Error({
                    statusCode: 409,
                    message: 'A user with this email already exists.'
                });
                throw err;
            }
            return findByIdAndUpdate(authUserId, user, {new: true})
        })
        .then(updatedUser => {
            if(!updatedUser){
                const err = new Error({
                    statusCode: 404,
                    message: 'Cannot find updated user.'
                })
                throw err;
            }
            res.status(200).send(updatedUser);
        })
        .catch(err => errors.responseError(err, res));
}


exports.deleteUser = (req, res, next) => {
    userId = mongoose.Types.ObjectId(req.params.userId);
    User
        .findByIdAndDelete(userId)
        .then(deletedUser=>{
            if(!deleteduser){
                const error = new Error();
                error.statusCode = 404;
                error.message = "Could not find the user to delete."
                throw error;
            }
            res.status(200).send(deleteduser);
        })
        .catch(err => errors.responseError(err, res))
}

exports.loginUser = (req, res, next) => {
    let loggedInUser;
    User
        .findOne({email: req.body.email})
        .then(user => {
            if(!user){
                const error = new Error();
                error.statusCode = 401;
                error.message = "Login failed.";
                throw error;
            }
            loggedInUser = {
                userId: user._id.toString(),
                userType: user.userType,
                email: user.email,
                status: user.status,
                application: user.application,
                profile: user.profile
            };
            return bcrypt.compare(req.body.password, user.password);
        })
        .then(matched => {
            if(!matched){
                const error = new Error();
                error.statusCode = 401;
                error.message = "Login failed";
                throw error;
            }
            const token = jwt.sign({
                userId: loggedInUser.userId,
                userType: loggedInUser.userType,
                status: loggedInUser.status
            }, 'DEVELOPMENT SECRET KEY TO CHANGE');
            res.status(200).json({token, userType: loggedInUser.userType, status: loggedInUser.status});
        })
        .catch(err => errors.responseError(err, res));
}

exports.findClientByEmail = (req, res, next) => {
    User
        .findOne({userType: globals.client, email: req.body.email}).populate('profile')
        .then(client => {
            if(!client){
                const err = new Error();
                err.statusCode = 404;
                throw err;
            }
            return Profile.findOne({userId: client._id}
        )})
        .then(client => {
            if(!client){
                const err = new Error();
                err.statusCode = 404;
                throw err;
            }
            res.status(200).send(client)
        })
        .catch(err => {
            errors.responseError(err, res);
        })
}