const mongoose = require('mongoose');

const errors = require('./error');

const globals = require('../globals');

const Project = require('../models/project');
const Bids = require('../models/bid');

exports.addProject = (req, res, next) => {
    const project = new Project(req.body);
    if(req.userType === globals.client){
        project.clientId = req.userId;
    }
    project.dateCreated = new Date();
    project
        .save()
        .then(result => res.status(201).send(result))
        .catch(err => errors.responseError(err, res));
}

exports.getProjects = (req, res, next) => {
    const authUserId = mongoose.Types.ObjectId(req.userId);
    Project
        .find({status: globals.open, 'supplierBids.SupplierId':{'$ne': req.userId}})
        .then(projects => res.status(200).send(projects))
        .catch(err => errors.responseError(err, res));
}

exports.getProject = (req, res, next) => {
    const projectId = mongoose.Types.ObjectId(req.params.projectId);
    Project
        .findById(projectId)
        .then(project => {
            // if(!project){
            //     const err = new Error();
            //     err.statusCode = 404;
            //     err.message = "Could not find this project.";
            //     throw err;
            // }
            res.status(200).send(project);
        })
        .catch(err => errors.responseError(err, res));
}

exports.getUsersProjects = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.userId);
    let filter;
    if(req.userType === globals.client){
        filter = {clientId: userId}
    }
    else if(req.userType === globals.supplier){
        filter = {supplierId: userId}
    }
    else{
        return errors.responseError({}, res)
    }
    Project
        .find(filter)
        .then(projects => res.status(200).send(projects))
        .catch(err => errors.responseError(err, res));
}

exports.getBookings = (req, res, next) => {
    const supplierId = mongoose.Types.ObjectId(req.userId);
    Bids
        .find({supplierId, status: globals.accepted}).populate({path:'projectId', select:'-supplierBids',  populate: {path:'clientId',select:'-status -password -_id -userType', populate:{path:'profile'}}})
        .then(bookings => {
            res.status(200).send(bookings)
        })
        .catch(err => errors.responseError(err, res))
}

exports.getBooking = (req, res, next) => {
    const supplierId = mongoose.Types.ObjectId(req.userId);
    const bidId = mongoose.Types.ObjectId(req.params.bidId)
    Bids
        .findOne({_id: bidId, supplierId, status: globals.accepted}).populate({path:'projectId', select:'-supplierBids',  populate: {path:'clientId', select:'-status -password -_id -userType',populate:{path:'profile'}}})
        .then(booking => {
            res.status(200).send(booking)
        })
        .catch(err => errors.responseError(err, res))
}

exports.editProject = (req, res, next) => {
    const projectId = mongoose.Types.ObjectId(req.params.projectId);
    Project
        .findByIdAndUpdate(projectId, req.body, {new: true, findAndModify: false})
        .then(updatedProject => {
            if(!updatedProject){
                const err = new Error();
                err.statusCode = 404;
                throw err
            }
            return res.status(200).send(updatedProject);
        })
        .catch(err => errors.responseError(err, res));
}

exports.deleteProject = (req, res, next) => {
    const projectId = mongoose.Types.ObjectId(req.params.projectId);
    Project
        .findByIdAndDelete(projectId)
        .then(deletedProject => {
            res.status(200).send();
        })
        .catch(err => errors.responseError(err, res));
}

