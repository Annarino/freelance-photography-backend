const path = require('path');
const fs = require('fs');

exports.getProposal = (req, res, next) => {
    const proposalPath = path.join('proposals', req.params.userId, req.params.filename);
    const proposal = fs.createReadStream(proposalPath);
    res.setHeader('Content-Type', 'application/pdf')
    res.setHeader('Content-Disposition', 'inline')
    proposal.pipe(res);
}