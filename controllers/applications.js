const mongoose = require('mongoose');

const error = require('./error');

const globals = require('../globals');

const Application= require('../models/application');
const User = require('../models/user');

exports.addApplication = (req, res, next) => {
    const newApplication = new Application(req.body);
    userId = mongoose.Types.ObjectId(req.userId);
    newApplication.status = 'pending';
    let userToUpdate;
    User
        .findById(userId)
        .then(user => {
            if(!user){
                const err = new Error({
                    statusCode: 404,
                    message: 'There was an error finding a user for this application.'
                })
            }
            newApplication.email = user.email;
            newApplication.userType = user.userType;
            newApplication.userId = user._id;
            return Application.findOne({userId: newApplication.userId})
        })
        .then(existingApplication => {
            if(existingApplication){
                const err = new Error();
                err.statusCode = 409;
                err.message = "An application already exists! Only one application per user is allowed.";
                throw err;
            }
            return newApplication.save();
        })
        .then(application => res.status(200).send(application))
        .catch(err => error.responseError(err, res));
}

exports.getSupplierApplication = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.userId);
     Application
        .findOne({userId})
        .then(application => {
            if(!application || application === null){
                const err = new Error({
                    statusCode: 404,
                    message: 'Application could not be found.'
                })
            }
            res.status(200).send(application)
        })
        .catch(err => error.responseError(err, res));
}

exports.getApplication = (req, res, next) => {
    applicationId = mongoose.Types.ObjectId(req.params.applicationId);
    Application
        .findById(applicationId)
        .then(application => {
            if(!application){
                const err = new Error();
                err.statusCode = 404;
                err.message = "An application with this ID could not be found."
            }
            res.status(200).send(application)})
        .catch(err => error.responseError(err, res));
}

exports.getApplicationsOfType = (req, res, next) => {
    Application
        .find({user:{userType:req.params.applicantType}})
        .then(applications => res.status(200).send(applications))
        .catch(err => error.responseError(err, res));
}

exports.getApplications = (req, res, next) => {
    Application
        .find()
        .then(applications => res.status(200).send(applications))
        .catch(err => error.responseError(err, res));
}

exports.editApplication = (req, res, next) => {
    userType = req.userType;
    userId = req.userId;
    let updatedApplication;
    let applicationId;
    if(userType === globals.administrator){
        applicationId = mongoose.Types.ObjectId(req.params.applicationId);
        updatedApplication = req.body;
    }else if(userType === globals.supplier){
        updatedApplication = {
            firstName: req.firstName,
            surname: req.surname,
            phoneNumber: req.phoneNumber,
            email: req.email,
            address: req.address,
            references: req.references,
            profileLinks: req.profileLinks
        };
        User
            .findById(userId)
            .then(user => applicationId = user.application)
            .catch(err => error.responseError(err, res));
    } else {
        const err = {
            statusCode: 401,
            message: 'Not authorised to update this application.'
        };
        res.status(401).json(err);
        return;
    }
    Application
        .findByIdAndUpdate(applicationId, updatedApplication, {useFindAndModify: false})
        .then( application => {
            if(!application){
                const err = new Error({
                    statusCode: 404,
                    message: "Could not find application to edit."
                })
                throw err;
            }
            return findById({_id: application._id})
        })
        .then(updatedApplication => res.status(200).send(updatedApplication))  
        .catch(err => error.responseError(err, res));
}


exports.deleteApplication = (req, res, next) => {
    applicationId = mongoose.Types.ObjectId(req.params.applicationId);
    Application
        .findByIdAndDelete(applicationId)
        .then(deletedApplication =>{
            if(!deletedApplication){
                const err = new Error({
                    statusCode: 404,
                    message: "Could not find user to delete."
                });
                throw err;
            }
            res.status(200).send(deletedApplication)
        })
        .catch(err => error.responseError(err, res));
}