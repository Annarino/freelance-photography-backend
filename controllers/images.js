const mongoose = require('mongoose');
const jimp = require('jimp');
const fs = require('fs');
const path = require('path')
const error = require('./error');

const Image = require('../models/image');
const User = require('../models/user');
const Profile = require('../models/profile');

exports.getUserImages = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.params.supplierId);
    Image
        .find({userId})
        .then(images => res.status(200).send(images))
        .catch(err => {
            console.log('Fetch users image err: ',err)
            error.responseError(err, res)})
}

exports.getImageLibrary = (req, res, next) => {
    const userId = mongoose.Types.ObjectId(req.userId)
    Image
        .find({userId})
        .then(images => {
            res.status(200).send(images)})
        .catch(err => {
            console.log('Image Supplier Fetch Err: ', err)
            error.responseError(err, res)
        })
}

exports.addImage = async (req, res, next) => {
    let incomingFiles = req.files;
    await Promise.all(
        incomingFiles.map(async file =>{
            const image = await jimp.read(file.path)
            await image.scaleToFit(1000,1000);
            await image.quality(60);
            await image.writeAsync('public/images/'+req.userId+'/'+file.originalname)
            await fs.unlink(file.path, (err) => { if(err){throw err;}})
        })
    )

    await Promise.all(
        incomingFiles.map(async file => {
            try{
                const image = new Image({
                    imageUrl: 'public/images/'+req.userId+'/'+file.originalname,
                    userId: req.userId,
                    tags:[]
                });
                await image.save();
                await res.status(201).send()
            }catch(err){
                error.responseError(err, res)
            }
        })
    )
}