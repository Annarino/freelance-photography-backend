const mongoose = require('mongoose');

const Invoice = require('../models/invoice');
const error = require('../controllers/error')

exports.getRevenue = (req, res, next) => {
    const supplierId = mongoose.Types.ObjectId(req.userId)
    let data = [];
    const amount = req.params.vatIncludedExcluded === 'excluded' ? 'subtotal' : 'total';
    Invoice
        .find({supplierId, status: 'paid'})
        .then(invoices => {
            invoices.map(invoice => {
                data.push({x: invoice.paymentDate.toJSON(), y: req.params.vatIncludedExcluded === 'excluded' ? invoice.subtotal : invoice.total})
            })
            return res.status(200).send(data)
        })
        .catch(err => {
            console.log('fetchRevenueErr: ', err);
            error.responseError(err, res)
        })


}