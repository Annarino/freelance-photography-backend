//Node Modules
const path = require('path');
const http = require('http');

//3rd Party Modules
const auth = require('./middleware/authorisation');
const fileReader = require('./controllers/files')
const fileHandlers = require('./middleware/fileHandlers')
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//Configuration variables/objects
const MONGODB_URI = 'CHANGE ME';


//Initialise express
const app = express();

//Middlewares
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    next();
});


//Routing Middlewares
const userRoutes = require('./routes/user');
const profileRoutes = require('./routes/profile');
const invoiceRoutes = require('./routes/invoice');
const projectRoutes = require('./routes/project');
const applicationRoutes = require('./routes/application');
const reviewRoutes = require('./routes/review');
const bidRoutes = require('./routes/bid');
const imageRoutes = require('./routes/image');
const dataPointRoutes = require('./routes/dataPoints')

app.use('/users', userRoutes);
app.use('/profiles', profileRoutes);
app.use('/invoices', invoiceRoutes);
app.use('/projects', projectRoutes);
app.use('/applications', applicationRoutes);
app.use('/bids', auth.authorize, fileHandlers.proposals, bidRoutes);
app.use('/reviews', reviewRoutes);
app.use('/images', imageRoutes);
app.use('/public', express.static(path.join(__dirname,'public/')))
app.use('/proposals/:userId/:filename', auth.authorize, fileReader.getProposal)
app.use('/data-points',auth.authorize, auth.isAdminOrSupplier, dataPointRoutes)

//Connect to MongoDB and start the server on specified port
const server = http.createServer(app);
mongoose.connect(MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true}).then(result => {
    console.log('connected');
    app.listen(8080);
}).catch(err => {
    console.log(err);
});